# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2001-2023, Python Software Foundation
# This file is distributed under the same license as the Python package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Python 3.11\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-21 14:55+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../Doc/distutils/packageindex.rst:7
msgid "The Python Package Index (PyPI)"
msgstr ""

#: ../Doc/distutils/packageindex.rst:9
msgid ""
"The `Python Package Index (PyPI)`_ stores metadata describing distributions "
"packaged with distutils and other publishing tools, as well the distribution "
"archives themselves."
msgstr ""

#: ../Doc/distutils/packageindex.rst:13
msgid ""
"References to up to date PyPI documentation can be found at :ref:`publishing-"
"python-packages`."
msgstr ""
