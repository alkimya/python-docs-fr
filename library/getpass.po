# Copyright (C) 2001-2018, Python Software Foundation
# For licence information, see README file.
#
msgid ""
msgstr ""
"Project-Id-Version: Python 3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-18 17:17+0200\n"
"PO-Revision-Date: 2018-09-28 12:39+0200\n"
"Last-Translator: Paquerette <paquereeette@riseup.net>\n"
"Language-Team: FRENCH <traductions@lists.afpy.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: library/getpass.rst:2
msgid ":mod:`getpass` --- Portable password input"
msgstr "Saisie de mot de passe portable"

#: library/getpass.rst:11
msgid "**Source code:** :source:`Lib/getpass.py`"
msgstr "**Source code:** :source:`Lib/getpass.py`"

#: includes/wasm-notavail.rst:3
msgid ":ref:`Availability <availability>`: not Emscripten, not WASI."
msgstr ""

#: includes/wasm-notavail.rst:5
msgid ""
"This module does not work or is not available on WebAssembly platforms "
"``wasm32-emscripten`` and ``wasm32-wasi``. See :ref:`wasm-availability` for "
"more information."
msgstr ""

#: library/getpass.rst:17
msgid "The :mod:`getpass` module provides two functions:"
msgstr "Le module :mod:`getpass` fournit 2 fonctions :"

#: library/getpass.rst:21
msgid ""
"Prompt the user for a password without echoing.  The user is prompted using "
"the string *prompt*, which defaults to ``'Password: '``.  On Unix, the "
"prompt is written to the file-like object *stream* using the replace error "
"handler if needed.  *stream* defaults to the controlling terminal (:file:`/"
"dev/tty`) or if that is unavailable to ``sys.stderr`` (this argument is "
"ignored on Windows)."
msgstr ""
"Affiche une demande de mot de passe sans renvoyer d'écho. L'utilisateur est "
"invité en utilisant la string *prompt*, avec en valeur par défaut "
"``'Password: '``. Avec Unix, l'invite est écrite dans l'objet fichier "
"*stream* en utilisant si besoin le *replace error handler*. *stream* sera "
"par défaut le terminal de contrôle (:file:`/dev/tty`), ou si celui ci n'est "
"pas disponible ce sera ``sys.stderr`` (cet argument sera ignoré sur Windows)."

#: library/getpass.rst:28
msgid ""
"If echo free input is unavailable getpass() falls back to printing a warning "
"message to *stream* and reading from ``sys.stdin`` and issuing a :exc:"
"`GetPassWarning`."
msgstr ""
"Si aucune saisie en mode sans affichage n'est disponible, ``getpass()`` se "
"résoudra à afficher un message d'avertissement vers *stream*, puis lire "
"l'entrée depuis ``sys.stdin``, en levant une :exc:`GetPassWarning`."

#: library/getpass.rst:33
msgid ""
"If you call getpass from within IDLE, the input may be done in the terminal "
"you launched IDLE from rather than the idle window itself."
msgstr ""
"Si vous appelez *getpass* depuis IDLE, la saisie peut être faite dans le "
"terminal depuis lequel IDLE a été lancé, plutôt que dans la fenêtre d'IDLE."

#: library/getpass.rst:38
msgid "A :exc:`UserWarning` subclass issued when password input may be echoed."
msgstr ""
"Une sous classe d'exception :exc:`UserWarning` est levée quand le mot de "
"passe saisi pourrait être affiché."

#: library/getpass.rst:43
msgid "Return the \"login name\" of the user."
msgstr "Renvoie le *login name* de l'utilisateur."

#: library/getpass.rst:45
msgid ""
"This function checks the environment variables :envvar:`LOGNAME`, :envvar:"
"`USER`, :envvar:`LNAME` and :envvar:`USERNAME`, in order, and returns the "
"value of the first one which is set to a non-empty string.  If none are set, "
"the login name from the password database is returned on systems which "
"support the :mod:`pwd` module, otherwise, an exception is raised."
msgstr ""
"Cette fonction examine les variables d'environnement :envvar:`LOGNAME`, :"
"envvar:`USER`, :envvar:`LNAME` et :envvar:`USERNAME`, dans cet ordre, et "
"renvoie la valeur de la première qui a comme valeur une string non vide. Si "
"aucune des variables n'est renseignée, dans le cas de systèmes qui prennent "
"en charge le module :mod:`pwd`, le *login name* de la base de données des "
"mots de passes est renvoyé, pour les autres systèmes une exception est levée."

#: library/getpass.rst:52
msgid ""
"In general, this function should be preferred over :func:`os.getlogin()`."
msgstr "En général, préférez cette fonction à :func:`os.getlogin()`."
