# Copyright (C) 2001-2018, Python Software Foundation
# For licence information, see README file.
#
msgid ""
msgstr ""
"Project-Id-Version: Python 3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-23 14:38+0200\n"
"PO-Revision-Date: 2019-04-11 18:10+0200\n"
"Last-Translator: Jules Lasne <jules.lasne@gmail.com>\n"
"Language-Team: FRENCH <traductions@lists.afpy.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#: library/pwd.rst:2
msgid ":mod:`pwd` --- The password database"
msgstr ""

#: library/pwd.rst:10
msgid ""
"This module provides access to the Unix user account and password database.  "
"It is available on all Unix versions."
msgstr ""

#: includes/wasm-notavail.rst:3
msgid ":ref:`Availability <availability>`: not Emscripten, not WASI."
msgstr ""

#: includes/wasm-notavail.rst:5
msgid ""
"This module does not work or is not available on WebAssembly platforms "
"``wasm32-emscripten`` and ``wasm32-wasi``. See :ref:`wasm-availability` for "
"more information."
msgstr ""

#: library/pwd.rst:15
msgid ""
"Password database entries are reported as a tuple-like object, whose "
"attributes correspond to the members of the ``passwd`` structure (Attribute "
"field below, see ``<pwd.h>``):"
msgstr ""

#: library/pwd.rst:20
msgid "Index"
msgstr "Index"

#: library/pwd.rst:20
msgid "Attribute"
msgstr "Attribut"

#: library/pwd.rst:20
msgid "Meaning"
msgstr "Signification"

#: library/pwd.rst:22
msgid "0"
msgstr "0"

#: library/pwd.rst:22
msgid "``pw_name``"
msgstr "``pw_name``"

#: library/pwd.rst:22
msgid "Login name"
msgstr "Nom d’utilisateur"

#: library/pwd.rst:24
msgid "1"
msgstr "1"

#: library/pwd.rst:24
msgid "``pw_passwd``"
msgstr "``pw_passwd``"

#: library/pwd.rst:24
msgid "Optional encrypted password"
msgstr ""

#: library/pwd.rst:26
msgid "2"
msgstr "2"

#: library/pwd.rst:26
msgid "``pw_uid``"
msgstr "``pw_uid``"

#: library/pwd.rst:26
msgid "Numerical user ID"
msgstr ""

#: library/pwd.rst:28
msgid "3"
msgstr "3"

#: library/pwd.rst:28
msgid "``pw_gid``"
msgstr "``pw_gid``"

#: library/pwd.rst:28
msgid "Numerical group ID"
msgstr ""

#: library/pwd.rst:30
msgid "4"
msgstr "4"

#: library/pwd.rst:30
msgid "``pw_gecos``"
msgstr "``pw_gecos``"

#: library/pwd.rst:30
msgid "User name or comment field"
msgstr ""

#: library/pwd.rst:32
msgid "5"
msgstr "5"

#: library/pwd.rst:32
msgid "``pw_dir``"
msgstr "``pw_dir``"

#: library/pwd.rst:32
msgid "User home directory"
msgstr "Répertoire d’accueil de l’utilisateur"

#: library/pwd.rst:34
msgid "6"
msgstr "6"

#: library/pwd.rst:34
msgid "``pw_shell``"
msgstr "``pw_shell``"

#: library/pwd.rst:34
msgid "User command interpreter"
msgstr ""

#: library/pwd.rst:37
msgid ""
"The uid and gid items are integers, all others are strings. :exc:`KeyError` "
"is raised if the entry asked for cannot be found."
msgstr ""

#: library/pwd.rst:44
msgid ""
"In traditional Unix the field ``pw_passwd`` usually contains a password "
"encrypted with a DES derived algorithm (see module :mod:`crypt`).  However "
"most modern unices  use a so-called *shadow password* system.  On those "
"unices the *pw_passwd* field only contains an asterisk (``'*'``) or the  "
"letter ``'x'`` where the encrypted password is stored in a file :file:`/etc/"
"shadow` which is not world readable.  Whether the *pw_passwd* field contains "
"anything useful is system-dependent.  If available, the :mod:`spwd` module "
"should be used where access to the encrypted password is required."
msgstr ""

#: library/pwd.rst:53
msgid "It defines the following items:"
msgstr ""

#: library/pwd.rst:58
msgid "Return the password database entry for the given numeric user ID."
msgstr ""

#: library/pwd.rst:63
#, fuzzy
msgid "Return the password database entry for the given user name."
msgstr ""
"Renvoie l'entrée de base de données de mot de passe *shadow* pour le nom "
"d'utilisateur donné."

#: library/pwd.rst:68
#, fuzzy
msgid ""
"Return a list of all available password database entries, in arbitrary order."
msgstr ""
"Renvoie une liste de toutes les entrées de la base de données de mots de "
"passe *shadow*, dans un ordre arbitraire."

#: library/pwd.rst:74
msgid "Module :mod:`grp`"
msgstr "Module :mod:`grp`"

#: library/pwd.rst:74
msgid "An interface to the group database, similar to this."
msgstr "Interface pour la base de données des groupes, similaire à celle-ci."

#: library/pwd.rst:76
#, fuzzy
msgid "Module :mod:`spwd`"
msgstr "Module :mod:`pwd`"

#: library/pwd.rst:77
#, fuzzy
msgid "An interface to the shadow password database, similar to this."
msgstr ""
"Interface pour la base de données (normale) des mots de passe, semblable à "
"ceci."

#: library/pwd.rst:42
msgid "module"
msgstr ""

#: library/pwd.rst:42
msgid "crypt"
msgstr ""
